#!/usr/bin/env python3

import os
import smtplib
import ssl
import config
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from jinja2 import Environment, FileSystemLoader

DIR  = os.path.dirname(__file__)
TEMPLATES_PATH = os.path.join(DIR, "templates/")

SENDER = config.USER
REPLY = config.REPLY
BCC = config.BCC


TEST_DATA = {"Subject": "Welcome to mail.coop", 
             "import_email": "cni@myphone.coop", 
             "givenname": "Kate", 
             "sn": "Dawson", 
             "email": "cni_test@mail.coop", 
             "password": "dadasdasdasdsadwww", 
             "ordernumber": "123456",
	     "template_txt": "welcome_email_2.txt.j2",
	     "template_html": "welcome_email_2.html.j2"
        }

'''
Bcc:  support@mail.coop, 
Subject:
import_email: legacy midcounties email address 
givenname:
sn: 
email:  new mailcoop mail address
password:
ordernumber
'''


def generate_content(data=TEST_DATA, template="welcome_email_1.txt.j2"):
    environment = Environment(loader=FileSystemLoader(TEMPLATES_PATH), comment_start_string='{=', comment_end_string='=}',)
    jinja_template = environment.get_template(template)
    return jinja_template.render(data)


# Create the plain-text and HTML version of your message

def generate_email(data=TEST_DATA):
    rcpt = data["import_email"]
    message = MIMEMultipart("alternative")
    message["Cc"] = data["email"]
    #message["Bcc"] = data["Bcc"]
    message["Subject"] = data["Subject"]
    message["From"] = SENDER
    message["To"] = rcpt 
    message["Reply-To"] = REPLY

    text = generate_content(data=data, template=data["template_txt"]) 
    html = generate_content(data=data, template=data["template_html"])


    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    return message


def send_email(data = TEST_DATA):
    # Create a secure SSL context
    context = ssl.create_default_context()
    message = generate_email(data)
    rcpt = data["email"].split(",") + config.BCC.split(",") + [data["import_email"]]
    
    with smtplib.SMTP_SSL(config.SMTP_SERVER, config.SMTP_PORT, context=context) as server:
        server.login(config.USER, config.USERPW)
        return_code = server.sendmail(
            SENDER, rcpt, message.as_string()
        )
    return return_code

def main():
    #message = generate_email()
    #print(message)
    response = send_email()
    print(response)
    
    

if __name__ == "__main__":
    main()
