#!/usr/bin/env ipython3

import migration_parser
import os
import shutil
import sys

ROOTPATH="/var/lib/docker/volumes/mailcowdockerized_vmail-vol-1/_data"

def main():
    if os.geteuid() != 0:
        print("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
        sys.exit()
    f = open(ROOTPATH + "/" + "touched_users.txt", "w")
    for data in migration_parser.generate_required_data_dict():
        f.write(data["email"] + "\n")
        f.write(data["import_email"] + "\n")

    print("Now do `doveadm quota recalc -F /var/vmail/touched_users.txt` in the dovecot container")


if __name__ == "__main__":
    main()

