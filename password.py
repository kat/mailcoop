#!/usr/bin/env python3
import os
import random
import string

DIR  = os.path.dirname(__file__)
WORDLIST = os.path.join(DIR, "wordlist/wordlist.txt")
CHARLIST = string.ascii_letters + string.digits + string.punctuation

def passphrase(wordlist=WORDLIST, number=4):
    with open(wordlist) as f:
        lines = f.read().splitlines()
    return "-".join([random.choice(lines) for i in range(number)])

def password(charlist=CHARLIST, number=12):
    return "".join([random.choice(CHARLIST) for i in range(number)])

def main():
    print(passphrase())
    print(password())


if __name__ == "__main__":
    main()


