#!/usr/bin/env python3

import api
import datareader
import os

DIR  = os.path.dirname(__file__)
CUMULATIVE_IMPORT_FILE = os.path.join(DIR, "imports/imported_cumulative.csv")

def is_failed(job):
    if job["exit_status"] == "EX_OK" or job["is_running"] == 1:
        rc = False
    else:
        rc = True
    return rc

def get_failing_sync_jobs():
    syncjobs = api.get_sync_jobs()
    return [job for job in syncjobs if is_failed(job)]

def get_all_mailboxes():
    mailboxes = api.get_mailbox(mailbox="all")
    return mailboxes

def get_users_no_sync_job():
    mailboxes  = api.get_mailbox("all")
    mailbox_users = [mailbox["username"] for mailbox in mailboxes]
    syncjobs = api.get_sync_jobs()
    syncjob_users = [syncjob["user2"] for syncjob in syncjobs]
    return list(set(mailbox_users) - set(syncjob_users))

def get_phonecoop_details_no_sync(import_file = CUMULATIVE_IMPORT_FILE):
    people = datareader.import_data_dict(import_file)
    local_parts = [user.split("@")[0] for user in get_users_no_sync_job()]
    nosync = [(local_part + "@mail.coop", person["import_email"],person["ordernumber"])  for local_part in local_parts for person in people if person["local_part"] == local_part]
    return nosync

def main():
    #for job in get_failing_sync_jobs():
    #    print(str(job["id"]) + "," + job["user1"] + "," + job["user2"] + "," + str(job["exit_status"])) 
    #for user in get_users_no_sync_job():
    #    print(user)
    #for person in get_phonecoop_details_no_sync():
    #    print(person)
    print("number of failing sync: ", len(get_failing_sync_jobs()))
    print("number of set up sync: ", len(api.get_sync_jobs()))
    print("number of mailboxes: ", len(get_all_mailboxes()))
        
if __name__ == "__main__":
    main()
