#!/usr/bin/env python3
import config, json, requests
from requests.exceptions import HTTPError

def get_domain(domain):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/domain/' + domain, headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def get_mailbox(mailbox):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/mailbox/' + mailbox, headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def does_mailbox_exist(mailbox):
    mailbox_info = get_mailbox(mailbox)
    return True if mailbox_info else False

def generate_mailbox_config(local_part='test', password='esdvsadfdrrewfgrfdds', full_name='test me', domain=config.LOCAL_DOMAIN, tags=[]):
    mailbox_config = {}
    mailbox_config['active'] = '1'
    mailbox_config['domain'] = domain
    mailbox_config['local_part'] = local_part
    mailbox_config['name'] = full_name
    mailbox_config['password'] = password
    mailbox_config['password2'] = password
    mailbox_config['quota'] = '5120'
    mailbox_config['force_pw_update'] = '0'
    mailbox_config['tls_enforce_in'] = '0'
    mailbox_config['tls_enforce_out'] = '0'
    mailbox_config['tags'] = tags
    return mailbox_config

def add_mailbox(mailbox_config):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json'
    headers['accept'] = 'application/json'
    try:
        r = requests.post(api_base + 'add/mailbox', headers=headers, data=json.dumps(mailbox_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def edit_mailbox(mailbox=[], attr={}):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json'
    headers['accept'] = 'application/json'
    try:
        r = requests.post(api_base + 'edit/mailbox', headers=headers, data=json.dumps({"items": mailbox, "attr": attr})) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()


def generate_alias_config(active="1", address="alias@example.org", goto="destination@example.com", sogo_visible="1"):
    return {"active": active, "address": address, "goto": goto, "sogo_visible": sogo_visible}

def add_alias(alias_config):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.post(api_base + 'add/alias', headers=headers, data=json.dumps(alias_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def generate_user_acl_config(mailbox):
    acl_config = {} 
    attr = {} 
    user_acl = [
      "spam_score",
      "spam_policy",
      "syncjobs",
      "quarantine",
      "sogo_profile_reset",
      "quarantine_attachments",
      "quarantine_notification",
    ]
    return {"attr": {"user_acl": user_acl}, "items": mailbox}

def edit_user_acl(mailbox="foo@mail.example", user_acl_config={}):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.post(api_base + 'edit/user-acl', headers=headers, data=json.dumps(user_acl_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def get_sync_jobs():
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/syncjobs/all/no_log', headers=headers)
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def deactivate_syncjobs(sync_id):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = {"items": sync_id, "attr": {"active": "0"}}
    try:
        r = requests.post(api_base + 'edit/syncjob', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def delete_syncjobs(sync_id):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = sync_id
    try:
        r = requests.post(api_base + 'delete/syncjob', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def set_tls_syncjobs(sync_id):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = {"items": sync_id, "attr": {"enc1": "TLS"}}
    try:
        r = requests.post(api_base + 'edit/syncjob', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def add_syncjob(syncjob_settings=None):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.post(api_base + 'add/syncjob', headers=headers, data=json.dumps(syncjob_settings))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def deactivate_mailbox(mailbox):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = {"items": mailbox, "attr": {"active": "0"}}
    try:
        r = requests.post(api_base + 'edit/mailbox', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def disallow_mailbox(mailbox):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = {"items": mailbox, "attr": {"active": "2"}}
    try:
        r = requests.post(api_base + 'edit/mailbox', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()


def set_mailbox_ratelimit(mailbox, rl_value="100", rl_frame="h"):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = {"items": mailbox, "attr": {"rl_value": rl_value, "rl_frame": rl_frame}}
    try:
        r = requests.post(api_base + 'edit/rl-mbox', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def get_fail2ban():
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/fail2ban/', headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()


def get_logs(service, count = 0):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    if count != 0:
        endpoint = service + "/" + str(count)
    else: 
        endpoint = service
    try:
        r = requests.get(api_base + 'get/logs/' + endpoint , headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def get_quarantine():
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/quarantine/all' , headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def delete_quarantine_item(qid):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    data = qid
    try:
        r = requests.post(api_base + 'delete/qitem', headers=headers, data=json.dumps(data))
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def edit_quarantine_notifications(mailbox=[], attr={"quarantine_notification": "never"}):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json'
    headers['accept'] = 'application/json'
    try:
        r = requests.post(api_base + 'edit/quarantine_notification', headers=headers, data=json.dumps({"items": mailbox, "attr": attr})) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def main():
    bob_mailbox = generate_mailbox_config(local_part = 'bob2', full_name = 'bob blueberry', password = 'asdfasdfasdfasdfasdf', tags=["order:0123456"])
    print(add_mailbox(bob_mailbox))
    #bob_alias = generate_alias_config (address="bob@test.example", goto="bob@mail.coop")
    #print(add_alias(bob_alias))
    #bob_acl_config = generate_user_acl_config('bob@mail.coop')
    #print(edit_user_acl(mailbox='bob@mail.coop',user_acl_config=bob_acl_config))
    #print(edit_user_acl(mailbox='bob@mail.coop'))

    print(get_sync_jobs())
    return

if __name__ == "__main__" :
    main()

