#!/usr/bin/env python3

import api
import config
import emailer
import datareader
import datetime
import imapsync
import password

def main():
    for data in datareader.generate_required_data_dict():
        dt = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        print(dt, data)
        if api.does_mailbox_exist(data["email"]) is not True:
            mailbox_config = api.generate_mailbox_config(local_part=data["local_part"], password=data["password"], full_name=data["full_name"], domain=data["domain"], tags=["created:" + str(dt),"ordernumber:" + str(data["ordernumber"])])
            mailbox_acl_config = api.generate_user_acl_config(data["email"])
            print(dt, api.add_mailbox(mailbox_config))
            user_acl_config = api.generate_user_acl_config(data["email"])
            print(dt, api.edit_user_acl(mailbox=data["email"],user_acl_config=user_acl_config))
            data["Subject"] = "LOGIN DETAILS FOR " + data["email"] + " Order: " + data["ordernumber"]  
            data['template_txt'] = 'support_email_details.txt.j2' 
            data["template_html"] = 'support_email_details.html.j2'
            print(dt, emailer.send_email(data))
            print(dt, imapsync.add_imapsync(data["email"]))
            #passwords = imapsync.read_password_file(domain=data["domain"])
            #if data["email"] in passwords:
            #    # print(mailbox, passwords[mailbox])
            #    imapsync_settings = imapsync.DEFAULT_SYNC_SETTINGS.copy()
            #    imapsync_settings["username"] = data["email"]
            #    imapsync_settings["user1"] = data["email"]
            #    imapsync_settings["password1"] = passwords[data["email"]]
            #    print(api.add_syncjob(imapsync_settings))
            #else: 
            #    print(data["email"], "NO PASSWORD")

        else:
            print(dt, "error cannot create: " + data["email"], "already exists")  
        
if __name__ == "__main__":
    main()
