#!/usr/bin/env python3
import api
import datetime

def unixtime_to_time(unixtime):
    return datetime.datetime.utcfromtimestamp(unixtime).strftime('%Y-%m-%d %H:%M:%S')



def main():
    mailboxes = api.get_mailbox("all")
    f = open("/home/kate/mailboxes.csv", "w")
    print("domain" + "," + "username" + ","  + "name" + "," + "active" + "," + "created" + "," + "quota" + ","  +  "tags",
    file=f)
    for m in mailboxes:
        username = ""
        domain = ""
        name = ""
        tags = ""
        active = ""
        created = ""
        quota = ""
        username = m["username"]
        domain = m["domain"]
        name = m["name"]
        active = m["active"]
        created = m["created"]
        quota = m["quota"]
        if "tags" in m:
            tags = ",".join(m["tags"])
        print(domain + "," + username + ","  + name + "," + str(active) + "," + str(created) + "," + str(quota) + ","  + tags,
    file=f)
    f.close()


if __name__ == "__main__":
    main()
