#!/usr/bin/env python3

import api
import config
import emailer
import migration_parser
import datetime
import password
import sql

def main():
    # get_sync_jobs is slow, so cache it here rather than calling it everytime
    all_sync_jobs = api.get_sync_jobs()
    for data in migration_parser.generate_required_data_dict():
        # make migration to mailbox

        dt = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        print(dt, data)
        if api.does_mailbox_exist(data["import_email"]) is not True:
            local_part, domain = data["import_email"].split("@")
            
            mailbox_config = api.generate_mailbox_config(local_part=local_part, domain=domain, password=data["password"], full_name=data["full_name"],tags=["created:" + str(dt),"ordernumber:" + str(data["ordernumber"])])
            mailbox_acl_config = api.generate_user_acl_config(data["import_email"])
            print(dt, api.add_mailbox(mailbox_config))
            user_acl_config = api.generate_user_acl_config(data["import_email"])
            print(dt, api.edit_user_acl(mailbox=data["import_email"],user_acl_config=user_acl_config))
        else:
            print(dt, "error cannot create: " + data["import_email"], "already exists")
        
        # Copy password hashes
        print(dt, "update destination password")
        password = sql.get_password_hash(username=data["email"])
        print(dt, sql.set_password_hash(password=password, username=data["import_email"]))

        # copy existing sync job settings... 
        #TODO not sure of this is best to do here or elsewhere
        #TODO should do this separately after doveadm task
        print(dt, "disable existing sync jobs")
        # job["id"] for job in  api.get_sync_jobs() if job["user1"]== data["import_email" and job["user2"]==data["email"]
        syncjobs = [job["id"] for job in all_sync_jobs if job["user2"]==data["email"]] # TODO this should just be active jobs
        #for sync_id in syncjobs:
        #    syncjob_settings = sql.get_imapsync_settings(sync_id=sync_id)
        #    syncjob_settings["username"] = data["import_email"]
        #    # Create the syncjob inactive
        #    print(dt, api.add_syncjob(syncjob_settings=syncjob_settings))
        print(dt, api.deactivate_syncjobs(syncjobs))
        print(dt, api.disallow_mailbox(data["email"]))
    
    print(dt, "Finished")

if __name__ == "__main__":
    main()
