#!/usr/bin/env python3

from bs4 import BeautifulSoup
HTML_TEMPLATE = "templates/welcome_email_9.1.html.j2"
TEXT_TEMPLATE = "templates/welcome_email_9.1.txt.j2"
f = open(HTML_TEMPLATE, "r")
html = f.read()
html
soup = BeautifulSoup(html, features="html.parser")

# kill all script and style elements
for script in soup(["script", "style"]):
    script.extract()    # rip it out
# get text
text = soup.get_text()
f = open(TEXT_TEMPLATE, "w")
f.write(text)
f.close()

