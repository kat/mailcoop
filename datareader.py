#!/usr/bin/env python3

import config
import csv
import password
import os 

DIR  = os.path.dirname(__file__)
IMPORT_FILE = os.path.join(DIR, "import.csv")

def import_data_dict(import_file = IMPORT_FILE):
    with open(import_file, encoding='utf-8-sig') as csvfile:
        data_import = csv.DictReader(csvfile)
        data = list(data_import)

    return data

def generate_required_data_dict(data = import_data_dict()):
    for row in data:
        # local_part and import_email are always lowercase
        row["local_part"] = row["local_part"].lower()
        row["import_email"] = row["import_email"].lower()
        row["password"] = password.passphrase()
        # check to make sure fullname isn't going to be blank
        if row["givenname"] == "" and row["sn"] == "":
            row["full_name"] = row["local_part"]
        else:
            row["full_name"] = row["givenname"] + " " + row["sn"]
        # We just use import_email for everything now
        row["email"] = row["import_email"]
        row["local_part"],row["domain"] = row["import_email"].split("@")[0],row["import_email"].split("@")[1]
        row["Subject"] = "Welcome to Mail Coop!"
        row["template_txt"] =  "welcome_email_9.1.txt.j2"
        row["template_html"] = "welcome_email_9.1.html.j2"
    return data


def main():
    print(generate_required_data_dict())

if __name__ == '__main__':
    main()
