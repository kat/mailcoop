#!/usr/bin/env python3
import parser as p
FILENAME = "all_rspamd_data_pass1"
#FILENAME = "rspamd_data_scores"
#SPAMHAUS_TOKENS = ["RBL_SPAMHAUS_CSS",
#                    "RBL_SPAMHAUS_SBL",
#                    "RBL_SPAMHAUS_XBL",
#                    "RECEIVED_SPAMHAUS_CSS",
#                    "RECEIVED_SPAMHAUS_DROP",
#                    "RECEIVED_SPAMHAUS_PBL",
#                    "RECEIVED_SPAMHAUS_SBL",
#                    "RECEIVED_SPAMHAUS_XBL",
#                    "SPAMHAUS_FAIL"]

SPAMHAUS_TOKENS = ["DBL",
                   "DBL_ABUSE",
                   "DBL_ABUSE_BOTNET",
                   "DBLABUSED_BOTNET_FULLURLS",
                   "DBLABUSED_MALWARE_FULLURLS",
                   "DBLABUSED_PHISH_FULLURLS",
                   "DBLABUSED_SPAM_FULLURLS",
                   "DBL_ABUSE_MALWARE",
                   "DBL_ABUSE_PHISH",
                   "DBL_ABUSE_REDIR",
                   "DBL_BLOCKED",
                   "DBL_BLOCKED_OPENRESOLVER",
                   "DBL_BOTNET",
                   "DBL_MALWARE",
                   "DBL_PHISH",
                   "DBL_PROHIBIT",
                   "DBL_SPAM",
                   "RBL_SPAMHAUS_CW_BCH",
                   "RBL_SPAMHAUS_CW_BTC",
                   "RBL_SPAMHAUS_CW_ETH",
                   "RBL_SPAMHAUS_CW_LTC",
                   "RBL_SPAMHAUS_CW_XMR",
                   "RBL_SPAMHAUS_CW_XRP",
                   "RBL_SPAMHAUS_HBL_URL",
                   "SH_HBL_EMAIL",
                   "SH_HBL_FILE_MALICIOUS",
                   "SH_HBL_FILE_SUSPICIOUS",
                   "SPAMHAUS_SBL_URL",
                   "SPAMHAUS_ZEN_URIBL",
                   "URIBL_DROP",
                   "URIBL_PBL",
                   "URIBL_SBL",
                   "URIBL_SBL_CSS",
                   "URIBL_XBL"]

def get_action(text):
    return text.split(":")[1].strip()

def get_score(text):
    score, reject  = p.chunk_text(text.split(":")[2],delim="[]")[0].split("/")
    return (float(score), float(reject))

def get_token_weight(token, text):
    token = token + "("
    token_length = len(token) - 1 
    try:
        token_start = text.index(token) + token_length - 1
        weight = float(p.chunk_text(text[token_start:token_start+10])[0])
    except ValueError:
        weight = 0
    return weight

def get_total_token_weights(tokens, text):
    total_weight = 0
    for token in tokens:
        total_weight += get_token_weight(token, text)
    return total_weight
    # return sum([get_token_weight(token, text) for token in tokens])

def main():
    with open(FILENAME) as scores:
        data = scores.readlines()
    for text in data:
        action = get_action(text)
        score, reject = get_score(text)
        token_weights = get_total_token_weights(SPAMHAUS_TOKENS, text)
        new_score = score - token_weights
        changed = False
        if score > reject:
            if new_score < reject:
                changed = True
        if score > 8:
            if new_score < 8:
                changed = True
        print(action, score, reject, token_weights, changed)

if __name__ == "__main__":
    main()
