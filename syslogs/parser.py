#!/usr/bin/env python3
'''
This module is for returning text inside delimiters
'''

TEST = "foo bar (Some text (and inside) and outside) then some (final text (asdsad))"

FILENAME = "all_rspamd_data"

'''
Reads lines of text and returns the index of outermost of a pair of matching delimiters
'''
def outer_delim(text, delim ="()"):
    out = []
    cnt = 0
    inside = False
    for i in range(len(text)):
        if text[i] == delim[0]:
            if not inside:
                start = i
            inside = True
            cnt += 1
        if text[i] == delim[1]:
            cnt -=1
            if cnt == 0 and inside:
                end = i+1
                inside = False
                out.append((start,end))
    return out

'''
Returns the text in the delimiters
'''
def chunk_text(text, delim="()"):
    out = []
    for index_pair in outer_delim(text, delim=delim):
        out.append(text[index_pair[0]+1:index_pair[1]-1])
    return out

def test():
    print(chunk_text(TEST))

def main():
    with open(FILENAME, "r") as rspamd_logs:
        data = rspamd_logs.readlines()
    for text in data:
        print(chunk_text(text)[1])

if __name__ == '__main__':
    main()
